import unittest
from app import app, db


class AppTestCase(unittest.TestCase):

    def setUp(self):
        self.app = app
        self.client = self.app.test_client()
        self.user = {'username': 'johncena', 'email': 'johncena@teko.vn'}

        with self.app.app_context():
            db.create_all()

    def test_user_creation(self):
        res = self.client.post('/api/user', data=self.user)
        self.assertEqual(res.status_code, 200)

    def test_user_read_all(self):
        res = self.client.get('/api/user')
        self.assertEqual(res.status_code, 200)

    def test_user_read(self):
        res = self.client.post('/api/user', data=self.user)
        res = self.client.get('/api/user/1')
        self.assertEqual(res.status_code, 200)

    def test_user_update(self):
        res = self.client.post('/api/user', data=self.user)
        new = {'username': 'johncena1', 'email': 'johncena1@teko.vn'}
        res = self.client.put('/api/user/1', data=new)
        self.assertEqual(res.status_code, 200)

    def test_user_delete(self):
        res = self.client.post('/api/user', data=self.user)
        res = self.client.delete('/api/user/1')
        self.assertEqual(res.status_code, 200)

    def tearDown(self):
        with self.app.app_context():
            db.session.remove()
            db.drop_all()


if __name__ == '__main__':
    unittest.main()
