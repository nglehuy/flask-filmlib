from config import Config
from flask import Flask, Blueprint
from flask_restful import Api
import app.models
from app.models import db, login
from app.user.api import UserAPI
from app.film.api import FilmAPI
from app.category.api import CategoryAPI
from app.country.api import CountryAPI
from app.rating.api import RatingAPI

# create blueprint
api_bp = Blueprint('api', __name__, url_prefix='/api')

app = Flask(__name__)
app.config.from_object(Config)

# api on blueprint
api = Api(api_bp)

db.init_app(app)
login.init_app(app)

# register blueprint to app
app.register_blueprint(api_bp)

# add resource
api.add_resource(UserAPI, '/user', '/user/<int:id>')
api.add_resource(FilmAPI, '/film', '/film/<int:id>')
api.add_resource(CategoryAPI, '/category', '/category/<int:id>')
api.add_resource(CountryAPI, '/country', '/country/<int:id>')
api.add_resource(RatingAPI, '/rating', '/rating/<int:id>')
