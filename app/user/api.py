from flask import request, jsonify, abort
from flask_restful import Resource
from app.models import User, db


class UserAPI(Resource):
    def get(self, id=None):
        if not id:
            users = User.query.all()
            res = {}
            for usr in users:
                res[usr.id] = {
                    'username': usr.username,
                    'email': usr.email
                }
        else:
            user = User.query.get(id)
            if not user:
                abort(404)
            res = {
                'username': user.username,
                'email': user.email
            }
        return jsonify(res)

    def post(self):
        username = request.form.get('username')
        email = request.form.get('email')
        new_user = User(username, email)
        db.session.add(new_user)
        db.session.commit()
        return jsonify({
            new_user.id: {
                'username': new_user.username,
                'email': new_user.email
            }
        })

    def put(self, id):
        user = User.query.get(id)
        username = request.form.get('username')
        email = request.form.get('email')
        user.username = username
        user.email = email
        db.session.commit()
        return jsonify({
            user.id: {
                'username': user.username,
                'email': user.email
            }
        })

    def delete(self, id):
        user = User.query.get(id)
        db.session.delete(user)
        db.session.commit()
        return jsonify({
            "message": "delete user {} successfully".format(user.username)
        })
