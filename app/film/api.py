from flask import request, jsonify, abort
from flask_restful import Resource
from app.models import Film, db


class FilmAPI(Resource):
    def get(self, id=None):
        if not id:
            films = Film.query.all()
            res = {}
            for f in films:
                res[f.id] = {
                    'name': f.name,
                    'category_id': f.category_id,
                    'country_id': f.country_id
                }
        else:
            film = Film.query.get(id)
            if not film:
                abort(404)
            res = {
                'name': film.name,
                'category_id': film.category_id,
                'country_id': film.country_id
            }
        return jsonify(res)

    def post(self):
        name = request.form.get('name')
        category_id = request.form.get('category_id')
        country_id = request.form.get('country_id')
        new_film = Film(name, category_id, country_id)

        db.session.add(new_film)
        db.session.commit()

        return jsonify({
            new_film.id: {
                'name': new_film.name,
                'category_id': new_film.category_id,
                'country_id': new_film.country_id
            }
        })

    def put(self, id):
        film = Film.query.get(id)
        name = request.form.get('name')
        category_id = request.form.get('category_id')
        country_id = request.form.get('country_id')

        film.name = name
        film.category_id = category_id
        film.country_id = country_id

        db.session.commit()
        return jsonify({
            film.id: {
                'name': film.name,
                'category_id': film.category_id,
                'country_id': film.country_id
            }
        })

    def delete(self, id):
        film = Film.query.get(id)

        db.session.delete(film)
        db.session.commit()

        return jsonify({
            "message": "delete film {} successfully".format(film.name)
        })
